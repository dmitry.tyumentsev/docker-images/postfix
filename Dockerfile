FROM debian:buster-slim

RUN adduser -disabled-password --gecos "" --shell /usr/sbin/nologin vmail

RUN echo "postfix postfix/main_mailer_type string 'No Configuration'" | debconf-set-selections
RUN apt-get update && apt-get install -y \
    postfix \
    dovecot-pop3d \
    dovecot-imapd \
    rsyslog \
    && rm -rf /var/lib/apt/lists/*

COPY scripts/create_user /usr/bin/

COPY configs/dovecot/* /etc/dovecot/
COPY configs/postfix/* /etc/postfix/

RUN chmod +x /usr/bin/create_user
RUN touch /var/log/mail.log

EXPOSE 25 143 587

HEALTHCHECK --interval=60s --timeout=15s \
    CMD echo QUIT | nc localhost 25 || exit 1

RUN > /etc/aliases
RUN newaliases

RUN > /etc/postfix/virtual
RUN postmap /etc/postfix/virtual

RUN service postfix reload

CMD service rsyslog start && service postfix start && service dovecot start && tail -F /var/log/mail.log
